let def =
{
    artifact_5: {
        quizType: "standar",
        rendering: "rendering_1",
        config: "procedural",
        title: "Hola Mundo",
        keyboardProfile: ["minimalist"],
        quiz: {
            generalLayout: "",
            formsQuestions: [
                {
                    layout: "horizontal",
                    nodes:
                        [

                            {
                                //tipo de pregunta
                                type: "mathfield",
                                //label,Answer,Validation
                                data: {
                                    label: `<math xmlns='http://www.w3.org/1998/Math/MathML'>
                                    <mrow>
                                     <mi>cos</mi>
                                     <mo>&#8289;</mo>
                                     <mo>(</mo>
                                     <msup>
                                      <mi>x</mi>
                                      <mn>3</mn>
                                     </msup>
                                     <mo>)</mo>
                                    </mrow>
                                   </math>`,
                                    condition: "raul",
                                    questionType: "basic"
                                }



                            },
                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data: {
                                    label: "raul2",
                                    condition: 1,
                                    optionList:
                                        [
                                            "",
                                            "Opcion 1",
                                            "Opcion 2",
                                            "Opcion 3"
                                        ],
                                    questionType: "basic"

                                }



                            },




                            {
                                //tipo de pregunta
                                type: "radio",
                                //label,Answer,Validation
                                data: {
                                    label: "¿Pregunta?",
                                    condition: "Sí",
                                    optionList:
                                        [

                                            "Sí",
                                            "No",

                                        ],
                                    questionType: "basic"

                                }



                            },

                            {
                                //tipo de pregunta
                                type: "radio",
                                //label,Answer,Validation
                                data: {
                                    label: "¿Pregunta?",
                                    condition: "Sí",
                                    optionList:
                                        [

                                            "Sí",
                                            "No",

                                        ],
                                    questionType: "basic"

                                }



                            },



                        ]
                },


                {
                    layout: "vertical",
                    nodes:
                        [

                            {
                                //tipo de pregunta
                                type: "mathfield",
                                //label,Answer,Validation
                                data: {
                                    label: "f(x)s",
                                    condition: "raul",
                                    questionType: "basic"
                                }



                            },
                            {
                                //tipo de pregunta
                                type: "mathfield",
                                //label,Answer,Validation
                                data: {
                                    label: "f(x)s",
                                    condition: "raul",
                                    questionType: "basic"
                                }



                            },


                        ]
                }

            ],

        }

    },

    artifact_6: {
        quizType: "table",
        rendering: "rendering_6",
        config: "procedural",
        //title: "Hola",
        keyboardProfile: ["minimalist"],
        quiz: {
            header: [





                [//row
                    {//cell 


                        nodes:
                            [
                                {
                                    //tipo de pregunta
                                    type: "text",
                                    //label,Answer,Validation
                                    data: "Hola ",
                                    formato: {
                                        fontSize:"1rem",
                                        fontStyle:"italic",
    
                                    }
                                }
                            ]
                    },
                             {//cell 


                                nodes:
                                [
                                    {
                                        //tipo de pregunta
                                        type: "text",
                                        //label,Answer,Validation
                                        data: "Hola ",
                                        formato: {
    
                                        }
                                    }
                                ]
                        },
                                 {//cell 


                                    nodes:
                                    [
                                        {
                                            //tipo de pregunta
                                            type: "text",
                                            //label,Answer,Validation
                                            data: `<math xmlns='http://www.w3.org/1998/Math/MathML'>
                                            <mrow>
                                             <mi>cos</mi>
                                             <mo>&#8289;</mo>
                                             <mo>(</mo>
                                             <msup>
                                              <mi>x</mi>
                                              <mn>3</mn>
                                             </msup>
                                             <mo>)</mo>
                                            </mrow>
                                           </math>`,
                                           formato: {
                                            fontSize:"0.9rem",
                                            fontStyle:"italic",
        
                                        }
                                        }
                                    ]
                            },
                                     {//cell 


                        nodes:
                        [
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: "Hola ",
                                formato: {

                                }
                            }
                        ]
                }
                ],



            ],
            cells:
                [
                    [// row
                        {//cell
                            nodes:
                                [
                                    {
                                        type: "simple",
                                        data: {
                                            label: "raul1",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },

                        {
                            nodes:
                                [
                                    {
                                        //tipo de pregunta
                                        type: "mathfield",
                                        //label,Answer,Validation
                                        data: {
                                            label: "f(x)s",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },
                        {
                            nodes:
                                [
                                    {
                                        type: "simple",
                                        data: {
                                            label: "raul1",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },
                        {
                            nodes:

                            {
                                type: "simple",
                                data: {
                                    label: "raul1",
                                    condition: "raul",
                                    questionType: "basic"
                                }
                            },




                        },
                    ],


                    [// row
                        {//cell
                            nodes:
                                [
                                    {
                                        //tipo de pregunta
                                        type: "text",
                                        //label,Answer,Validation
                                        data: "Hola como está mi nombre ",
                                        formato: {
                                            fontSize:"1rem",
                                            fontStyle:"italic",
        
                                        }
                                    }


                                ]
                        },

                        {
                            nodes:
                                [
                                    {
                                        type: "simple",
                                        data: {
                                            label: "raul1",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },
                        {
                            nodes:
                                [
                                    {
                                        type: "simple",
                                        data: {
                                            label: "raul1",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },
                        {
                            nodes:
                                [
                                    {
                                        type: "simple",
                                        data: {
                                            label: "raul1",
                                            condition: "raul",
                                            questionType: "basic"
                                        }
                                    },
                                    {
                                        type: "select",
                                        data: {
                                            label: "raul2",
                                            condition: 1,
                                            optionList:
                                                [
                                                    "",
                                                    "Opcion 1",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                                ],

                                        }



                                    },


                                ]
                        },
                    ]




                ],

        }

    },

    artifact_7: {
        quizType: "inline",
        rendering: "rendering_7",
        config: "procedural",
        keyboardProfile: ["minimalist"],
        // validationBehaviour:"sequential",

        quiz: {
            formsQuestions: [
                {
                    //layout: "horizontal",
                    nodes:
                        [

                            {
                                //tipo de pregunta
                                type: "simple",
                                //label,Answer,Validation
                                data: {
                                    label: "raul1",
                                    condition: "raul",
                                    questionType: "basic"
                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: "Hola Puede pasar",
                                formato: {
                                    fontSize:"1rem",
                                    fontStyle:"italic",

                                }
                            },
                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data: {
                                    label: "raul2",
                                    condition: 1,
                                    optionList:
                                        [
                                            "",
                                            "Opcion 1",
                                            "Opcion 2",
                                            "Opcion 3"
                                        ],
                                    questionType: "basic"

                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: "Hola Puede pasar",
                                formato: {
                                    fontSize:"1rem",
                                    fontStyle:"italic",

                                }
                            },
                            {
                                //tipo de pregunta
                                type: "radio",
                                //label,Answer,Validation
                                data: {
                                    label: "¿Pregunta?",
                                    condition: "Sí",
                                    optionList:
                                        [

                                            "Sí",
                                            "No",

                                        ],
                                    questionType: "basic"

                                }



                            },



                        ]
                }
            ]
        }
    },


        artifact_8: {
            quizType: "table",
            rendering: "rendering_8",
            // config: "procedural",
            title: "Hola",
            keyboardProfile: ["minimalist"],
            quiz: {
                header: [





                    [//row
                        {//cell 


                            nodes:
                                [
                                    {
                                        //tipo de pregunta
                                        type: "text",
                                        //label,Answer,Validation
                                        data: "Hola ",
                                        formato: {
                                            fontSize:"1rem",
                                            fontStyle:"italic",
        
                                        }
                                    }
                                ]
                        },
                                 {//cell 


                                    nodes:
                                    [
                                        {
                                            //tipo de pregunta
                                            type: "text",
                                            //label,Answer,Validation
                                            data: "Hola ",
                                            formato: {
        
                                            }
                                        }
                                    ]
                            },
                                     {//cell 


                                        nodes:
                                        [
                                            {
                                                //tipo de pregunta
                                                type: "text",
                                                //label,Answer,Validation
                                                data: `<math xmlns='http://www.w3.org/1998/Math/MathML'>
                                                <mrow>
                                                 <mi>cos</mi>
                                                 <mo>&#8289;</mo>
                                                 <mo>(</mo>
                                                 <msup>
                                                  <mi>x</mi>
                                                  <mn>3</mn>
                                                 </msup>
                                                 <mo>)</mo>
                                                </mrow>
                                               </math>`,
                                               formato: {
                                                fontSize:"0.9rem",
                                                fontStyle:"italic",
            
                                            }
                                            }
                                        ]
                                },
                                         {//cell 


                            nodes:
                            [
                                {
                                    //tipo de pregunta
                                    type: "text",
                                    //label,Answer,Validation
                                    data: "Hola ",
                                    formato: {

                                    }
                                }
                            ]
                    }
                    ],



                ],
                cells:
                    [
                        [// row
                            {//cell
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },

                            {
                                nodes:
                                    [
                                        {
                                            //tipo de pregunta
                                            type: "mathfield",
                                            //label,Answer,Validation
                                            data: {
                                                label: "f(x)s",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },
                            {
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },
                            {
                                nodes:

                                {
                                    //tipo de pregunta
                                    type: "radio",
                                    //label,Answer,Validation
                                    data: {
                                        label: "¿Pregunta?",
                                        condition: "Sí",
                                        optionList:
                                            [
    
                                                "Sí",
                                                "No",
    
                                            ],
                                        questionType: "basic"
    
                                    }
    
    
    
                                },



                            },
                        ],


                        [// row
                            {//cell
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },

                            {
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },
                            {
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },
                            {
                                nodes:
                                    [
                                        {
                                            type: "simple",
                                            data: {
                                                label: "raul1",
                                                condition: "raul",
                                                questionType: "basic"
                                            }
                                        },
                                        {
                                            type: "select",
                                            data: {
                                                label: "raul2",
                                                condition: 1,
                                                optionList:
                                                    [
                                                        "",
                                                        "Opcion 1",
                                                        "Opcion 2",
                                                        "Opcion 3"
                                                    ],

                                            }



                                        },


                                    ]
                            },
                        ]




                    ],

            }

        },
    
}

class QuizGenerator {
    constructor(definition) {
        this.definition = definition
        this.autoIncrement = 0
    }

    generateObject() { //Objeto contenedor de todas las preguntas

        let artifact
        let artifactArguments = {
            "key": undefined, //artifactName
            "artifactObject": undefined, //Artifact Object
            "value": undefined, //values to ArtifactObject
            //"value.keyboardProfile": value.keyboardProfile
        }

        for (let [key, value] of Object.entries(this.definition)) {
            artifactArguments.key = key //nombre del artefacto
            artifactArguments.value = value

            switch (value.quizType) {

                case "standar":


                    artifactArguments.artifactObject = new Contenedor()
                    //console.log(artifactArguments)

                    this.createAndAddQuestion(artifactArguments)
                    //                    this.createAndAddQuestion(key, artifact, value, value.keyboardProfile)


                    break;
                case "table":
                    //  console.log(artifactArguments.value.quiz.cells)
                    artifactArguments.artifactObject = new ContenedorCompuesto()


                    artifactArguments.artifact = artifact
                    this.createAndAddQuestion(artifactArguments)
                    // this.createAndAddQuestion(key, artifact, value, value.keyboardProfile)

                    break;
                case "inline":

                    artifactArguments.artifactObject = new ContenedorInline()
                    //       artifactArguments.artifact = artifact

                    this.createAndAddQuestion(artifactArguments)

                    break;
                // ...
                default:

                    console.log("default standar")
                // Código a ejecutar si no hay ningún caso que coincida con el valor de expression
            }
        }

    }

    createAndAddQuestion(artifact) {
        // console.log(2)
        let QuestionsData = {
        }
        let sectionData = {
        }

        QuestionsData.keyboardProfile = artifact.value.keyboardProfile

        artifact.artifactObject.createContainer(artifact)
        if (artifact.value.quiz?.formsQuestions) {

            artifact.value.quiz.formsQuestions.forEach((element, idxNumber) => {

                if (element.layout != undefined || element.layout != '') {

                    sectionData.layout = element.layout
                    sectionData.idxNumber = idxNumber
                    sectionData.artifactName = artifact.key
                    sectionData.section = artifact.artifactObject.createSection(sectionData)


                }



                QuestionsData.element = element
                QuestionsData.artifactName = artifact.key
                QuestionsData.artifactObject = artifact.artifactObject
                QuestionsData.section = sectionData.section
                artifact.QuestionsData = QuestionsData

                this.generateQuestion(QuestionsData)


            });



        }
        else {
            QuestionsData.artifactObject = artifact.artifactObject
            QuestionsData.artifactName = artifact.artifactObject.key
            QuestionsData.element = artifact.value.quiz.cells
            QuestionsData.header = artifact.value.quiz.header
            this.generateQuestion(QuestionsData)


        }


    }
    generateQuestion(artifactArguments) {


        //console.log(3)

        if (artifactArguments.element?.nodes) {

            this.generateQuestionsFromNodes(artifactArguments)

        }
        else {

            if (artifactArguments?.header) {


                this.generateQuestionsFromArray(artifactArguments, true)

            }
            else {
                this.generateQuestionsFromArray(artifactArguments,false)

            }
        }
    }

    questionFilter(QuestionElement) {
        let question
        switch (QuestionElement.type) {


            case "simple":

                question = new PreguntaSimple(QuestionElement)
                //quiestionData.question = question
                // this.addToContainerProcess(artifactArguments.artifact, question)
                // this.addToContainerProcess(quiestionData)
                return question

                break

            case "select":

                question = new Select(QuestionElement)
                //quiestionData.question = question
                // this.addToContainerProcess(artifactArguments.artifact, question)
                // this.addToContainerProcess(quiestionData)
                return question

                break
            //return question

            case "mathfield":
                // question = new MathField("mathfield", element.data[1], element.data[2], artifactArguments.value.keyboardProfile);
                question = new MathField(QuestionElement);
                //  quiestionData.question = question

                //   this.addToContainerProcess(quiestionData)
                return question

                break

            case "text":

                // console.log("element",element.data)
                question = new TextoGenerator(QuestionElement);
                //quiestionData.question = question
                return question

                // this.addToContainerProcess(artifactArguments.artifact, question)

                break


            case "radio":
                console.log()
                question = new Radio(QuestionElement);
                //quiestionData.question = question
                return question

                // this.addToContainerProcess(artifactArguments.artifact, question)

                break


            //return question





        }


    }
    generateQuestionsFromNodes(artifactArguments) {
        let dataToBuild = {

        }
        artifactArguments.element.nodes.forEach((questionNode) => {
            questionNode.artifactName = artifactArguments.artifactName
            questionNode.questionId = `${artifactArguments.artifactObject.artifactName}_question_${this.autoIncrement}`
            dataToBuild.questionObject = this.questionFilter(questionNode)
            dataToBuild.artifactObject = artifactArguments.artifactObject
            dataToBuild.layout = artifactArguments.layout
            dataToBuild.artifactName = artifactArguments.artifactName
            dataToBuild.innerContainer = artifactArguments.section
            dataToBuild.artifactObject.addComponente(dataToBuild)

            dataToBuild = {

            }
            this.autoIncrement += 1

        })
    }
    generateQuestionsFromArray(artifactArguments, isHeader = null) {

        console.log(artifactArguments)
        let dataToBuild = {

        }

        dataToBuild.keyboardProfile =arguments.keyboardProfile
        let arrayOfNodes = []

        if (isHeader === true) {
            artifactArguments.header.forEach((setOfQuestionRows) => {
                let arrayOfRow = []


                setOfQuestionRows.forEach((questionNodeSet) => {
                    let arrayOfCols = []

                    if (Array.isArray(questionNodeSet.nodes)) {

                        questionNodeSet.nodes.forEach((questionNode) => {

                            dataToBuild.questionObject = this.questionFilter(questionNode)
                            arrayOfCols.push(dataToBuild)
                            dataToBuild = {

                            }
                        })
                    }
                    else {

                        questionNodeSet.nodes.artifactName = artifactArguments.artifactName

                        dataToBuild.questionObject = this.questionFilter(questionNodeSet.nodes)

                        arrayOfCols.push(dataToBuild)
                        dataToBuild = {

                        }
                    }


                    arrayOfRow.push(arrayOfCols)



                });

                arrayOfNodes.push(arrayOfRow)

            })

        artifactArguments.artifactObject.addHeaderComponent(arrayOfNodes,true)
        arrayOfNodes=[]
        }
    

        artifactArguments.element.forEach((setOfQuestionRows) => {
            let arrayOfRow = []


        setOfQuestionRows.forEach((questionNodeSet) => {
                let arrayOfCols = []

                if (Array.isArray(questionNodeSet.nodes)) {

                    questionNodeSet.nodes.forEach((questionNode) => {
questionNode.keyboardProfile = artifactArguments.keyboardProfile
                        dataToBuild.questionObject = this.questionFilter(questionNode)
                        arrayOfCols.push(dataToBuild)
                        dataToBuild = {

                        }
                    })
                }
                else {

                    questionNodeSet.nodes.artifactName = artifactArguments.artifactName

                    dataToBuild.questionObject = this.questionFilter(questionNodeSet.nodes)

                    arrayOfCols.push(dataToBuild)
                    dataToBuild = {

                    }
                }


                arrayOfRow.push(arrayOfCols)



            });
            arrayOfNodes.push(arrayOfRow)
        })
        artifactArguments.artifactObject.addHeaderComponent(arrayOfNodes,false)


        

    }


    addToContainerProcess(quiestionData) {
        //console.log(quiestionData)

        // quiestionData.artifact.addComponente(quiestionData.questionObject, quiestionData.aditionalContainer)




    }
}
quizGen = new QuizGenerator(def)
quizGen.generateObject()
