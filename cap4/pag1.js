def = {
    artifact_10: {
        quizType: "inline",
        rendering: "rendering_10",
        keyboardProfile: ["minimalist"],
        size:'QContainer--langer',
        validationBehaviour: "sequential",

        quiz: {
            formsQuestions: [
                {
                    //layout: "horizontal",
                    nodes:
                        [
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: "¿Qué se hace si en la calculadora aparece el símbolo ! E ! ? _ :",
                                formato: {

                                }
                            },
                                        {
                                            //tipo de pregunta
                                            type: "select",
                                            //label,Answer,Validation
                                            data:{
                                                label:"raul2",
                                                condition:1,
                                                optionList:
                                                 [
                                                    "",
                                                    "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                                    "Opcion 2",
                                                    "Opcion 3"
                                            ],
                                            questionType:"basic"
            
                                            }
            
            
            
                                        },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Probablemente Ud tenga curiosidad por descubrir cuales son las curvas que corresponden a las teclas de su
                                calculadora. Las páginas que siguen son para ello. Las formas y características de las curvas de las teclas
                                son muy importantes para desarrollar métodos de graficación de las fórmulas más sofisticados y menos
                                tediosos que el "punto a punto".`,
                                formato: {

                                }
                            },



                        ]
                }
            ]
        }
    }
}



quizGen = new QuizGenerator(def)
quizGen.generateObject()