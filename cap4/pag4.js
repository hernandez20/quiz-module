def = {
    artifact_10: {
        quizType: "inline",
        rendering: "rendering_10",
        config: "procedural",
        keyboardProfile: ["minimalist"],
        size:'QContainer--langer',
        validationBehaviour: "sequential",

        quiz: {
            formsQuestions: [
                {
                    //layout: "horizontal",
                    nodes:
                        [
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: "Dominio",
                                formato: {

                                }
                            },
                                                                    {
                                //tipo de pregunta
                                type: "mathfield",
                                //label,Answer,Validation
                                data:{
                                    label:"f(x)s",
                                    condition:"raul",
                                    questionType:"basic"
                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Rango`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Puntos de corte con el eje x`,
                                formato: {

                                }
                            },

                            {
                                                                    type: "simple",
                                                                    data: {
                                                                        label: "raul1",
                                                                        condition: "raul",
                                                                        questionType: "basic"
                                                                    }
                                                                },

                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Puntos de corte con el eje y`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },

                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Max Abs`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },

                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Alcanzado en`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },

                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Min Abs`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Alcanzado en`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },



                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Min Rel`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },
                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Alcanzado en`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },

                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Zonas de Crecimiento`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },


                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Zonas de decrecimiento`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },



                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Partes positivas`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },


                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Partes negativas`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },


                            {
                                //tipo de pregunta
                                type: "text",
                                //label,Answer,Validation
                                data: `Conexidad`,
                                formato: {

                                }
                            },

                            {
                                //tipo de pregunta
                                type: "select",
                                //label,Answer,Validation
                                data:{
                                    label:"raul2",
                                    condition:1,
                                    optionList:
                                     [
                                        "",
                                        "El número es demasiado grande o demasiado pequeño para verse en el formato de la calculadora.",
                                        "Opcion 2",
                                        "Opcion 3"
                                ],
                                questionType:"basic"

                                }



                            },
                        ]
                }
            ]
        }
    }
}



quizGen = new QuizGenerator(def)
quizGen.generateObject()