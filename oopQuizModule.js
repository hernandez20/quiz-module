// Utilizando el enfoque de clases de ES6

//Clase Pregunta Clase principal
class Componente {
  validar() {
    // Implementación común de la validación
  }
  }

  class Pregunta extends Componente {
    constructor(texto) {
      super();
      this.texto = texto;
      this.validacion = false;
    }
  }

  class PreguntaSimple extends Pregunta {
    constructor(texto) {
      super(texto);
      this.validacion = false;
    }
  
    validar() {
      // Implementación de validación específica para preguntas simples
    }
  }




  // Clase para contenedores (tabla, div, etc.)
class Contenedor extends Componente {
  constructor() {
    super();
    this.preguntas = [];
  }

  agregarComponente(componente) {
    this.preguntas.push(componente);
  }

  validar() {
    // Implementación de validación de contenedores
  }
}

// Clase para contenedores compuestos (tabla)
class ContenedorCompuesto extends Contenedor {
  constructor() {
    super();
    this.preguntas = [];
  }

  agregarComponente(componente) {
    this.preguntas.push(componente);
  }

  validar() {
    // Implementación de validación de contenedores compuestos
  }
}


// Ejemplo de uso
const preguntaSimple = new PreguntaSimple("Nombre");
const preguntaCompuesta = new ContenedorCompuesto();
preguntaCompuesta.agregarComponente(preguntaSimple);
console.log(preguntaSimple)