

class Contenedor {
  constructor() {
    this.preguntas = []
    this.isChanged = false
    this.interactionTime
    this.seconds = 0

    this.mainCard;

    this.config = null
this.modal;

    this.cotentContainer = document.createElement("div")
    this.keyBoardProfile = "default"
    this.qualifySumary = {
      total: 0,
      correct: 0,
      incorrect: 0,
      forAnswer: 0
    }

    //  this.preguntas = [];
    //this.keyBoardProfile = "default"



    this.validation = null
    this.contenedor = null
    this.lastNode = null
    this.result = {}
    this.updateValorChange = this.updateValor.bind(this);


  }
  defModal() {

    if (!(document.querySelector('.modal-container'))) {
      this.modal = document.createElement('div')
      this.modal.classList.add('modal-container')
      this.modal.innerHTML =`
      
        <div class="modal">
          <div class="modal_header">
            <p>¿Pantalla Pequeña? Agrega tu respuesta aquí: </p>
          </div>
          <div class ="modal_question">
          </div>
        </div>
    
    `
      let modalReference = this.modal

      document.querySelector('.container') ? document.querySelector('.container').appendChild(this.modal): document.body.appendChild(this.modal);

      this.modal.addEventListener('click', function (event) {

        if (event.target.classList[0] == 'modal-container') {
          modalReference.style.display = 'none';
          // modalReference.querySelector('.modal').removeChild(modalReference.querySelector('.modal').firstChild)
        }
      });

      // this.modal.style.display = 'none';

      // this.modal.addEventListener('click', (e) => {
      //   this.showQuestionModal()

      // })


    }
    else {
      console.log('Modal Ya definido')
    }


  }

  // Función para esconder el modal

  updateValor(value) {
    this.isChanged = value;
  }

  validationProcess() {
    if (this.isChanged === true){

    this.qualifySumary.total = this.preguntas.length
    this.qualifySumary.correct = 0
    this.qualifySumary.incorrect = 0
    this.qualifySumary.forAnswer = 0

    this.preguntas.forEach(questionElement => {
      let evaluate = questionElement.questionObject.validate()
      if (evaluate === true) {
        this.qualifySumary.correct ++

      }
      else if (evaluate === false) {
        this.qualifySumary.incorrect ++
      }
      else if (evaluate == undefined) {
        this.qualifySumary.forAnswer ++
      }


    });


   
    this.beforeToSend()
    this.updateValor(false)
  }
  else{
    console.warn("no ha habido algun cambio, Por lo tanto no se enviará otra interacción")
  }

}

  resetProcess() {

    this.qualifySumary.total = this.preguntas.length
    this.qualifySumary.correct = 0
    this.qualifySumary.incorrect = 0
    this.qualifySumary.forAnswer = 0

    this.preguntas.forEach(questionElement => {

      let evaluate = questionElement.questionObject.reset()
    });

  }

  createContainer(artifactArguments) {
    this.config = artifactArguments.value?.config

    this.keyBoardProfile = artifactArguments.keyBoardProfile


    this.contenedor = artifactArguments.value.rendering !== undefined
      ? artifactArguments.value.rendering
      : 'container'

    this.artifactName = artifactArguments.key
    this.interval;
    document.addEventListener('click', function (e) {
      const card = e.target.closest('.QContainer');

      if (card && e.target.closest('[id]').id === this.artifactName && e.target.classList.contains('button__validation--validate')) {
        this.validationProcess()
      }
      else if (card && e.target.closest('[id]').id === this.artifactName && e.target.classList.contains('button__validation--reset')) {
        this.resetProcess()
      }
      e.stopPropagation();

    }.bind(this)); // Establece el valor de 'this' en el objeto específico

    this.mainCard = document.createElement('div');
    this.mainCard.classList.add("QContainer")


    if (artifactArguments.value?.size) {
      this.mainCard.classList.add(artifactArguments.value.size)
    }
    this.mainCard.setAttribute("id", this.artifactName)



    this.titleContainer = document.createElement('div');
    this.titleContainer.classList.add("QContainer_tittle")

    if (artifactArguments.value.title != undefined) {
      this.titleContainer.textContent = artifactArguments.value.title

    }


    this.mainCard.appendChild(this.titleContainer)



    this.cotentContainer.classList.add("quiz")
    this.mainCard.appendChild(this.cotentContainer)

    if (artifactArguments.value.quizType == "table") {
      this.cotentContainer.classList.add("QContainer_table")

    }

    this.buttonsContainer = document.createElement("div");
    this.buttonsContainer.classList.add("buttons")





    this.buttonReset = document.createElement("div")
    this.buttonReset.classList.add("button", "button__validation", "button__validation--reset")
    this.buttonReset.textContent = '↺ '
    this.buttonsContainer.appendChild(this.buttonReset)

    this.buttonSuccess = document.createElement("div")
    this.buttonSuccess.classList.add("button", "button__validation", "button__validation--validate")
    this.buttonSuccess.textContent = "✓"

    this.buttonsContainer.appendChild(this.buttonSuccess)


    this.mainCard.appendChild(this.buttonsContainer)



    this.footer = document.createElement("div")
    this.footer.classList.add("QContainer_footer");

    this.mainCard.appendChild(this.footer)

    document.querySelector("#" + `${this.contenedor}`)
      ? document.querySelector("#" + `${this.contenedor}`).appendChild(this.mainCard)
      : document.querySelector("." + `${this.contenedor}`)
        ? document.querySelector("." + `${this.contenedor}`).appendChild(this.mainCard)
        : document.body.appendChild(this.mainCard);

    this.mainCard.addEventListener('mouseenter', (e) => {

      this.Gtime()
    })



    this.mainCard.addEventListener('mouseleave', (e) => {
      clearInterval(this.interval);
    })


  }

  Gtime() {
    this.interval = setInterval(() => {

      this.seconds++;


    }, 1000);
    //return this.seconds
  }

  procedural(step, preguntas, componente = null) {

    switch (step) {
      case 1:

        if (preguntas.length < 1) {
          componente.questionObject.isValidable = true
          this.lastNode = componente
        }
        else {

          componente.questionObject.isValidable = false
        }

        break;
      case 2:

        if (preguntas.length > 0) {
    
          if (this.lastNode == null) {
            this.lastNode = componente

          }
          else {
            try {

              this.lastNode.questionObject.metodoEnlazado = this.lastNode.questionObject.nextNodeTrigger.bind(componente.questionObject)

              this.lastNode = componente

            } catch (error) {
              console.log(error)
            }



          }


        }

      default:
        break;
    }

  }

  createSection(QuestionsData) {
    let section = document.createElement("div")
    section.setAttribute("id", `${QuestionsData.artifactName}_section_${QuestionsData.key}`)
    section.classList.add('quiz_section', `quiz_section--${QuestionsData.layout}`)

    this.cotentContainer.appendChild(section)
    return section
  }

  addComponente(componente) {
    let arrayAux = []
    let callProcedural = false;
    if (this.config === 'procedural') {
      callProcedural = true;
    }




    const tiposDeseados = ['math-field', 'textfield', 'select', 'input[type="radio"]:checked', "input"];
    componente.questionObject.callProcedural =callProcedural

    if (callProcedural) {
      this.procedural(1, this.preguntas, componente)
    }

    let nodoData = componente.questionObject.crearNodo()


    if ((componente.innerContainer != undefined) || (componente.innerContainer != null)) {

      componente.innerContainer.appendChild(nodoData[0])
    }
    else {

      this.cotentContainer.appendChild(nodoData[0])
    }


    tiposDeseados.forEach((tipo) => {
      const nodoHijo = this.cotentContainer.querySelector(tipo);

      if (nodoHijo != null) {
        if (callProcedural) {
          this.procedural(2, this.preguntas, componente)
        }

        arrayAux.push(nodoHijo)
        arrayAux.push(nodoData[1])






      }



    })

    componente.questionObject.updateValorIsChange(this)

    this.preguntas.push(componente);
    arrayAux = []
  }
  setValidationMessage(message) {
    this.footerText = message
  }
  getValidationMessage(message) {
    this.footer.textContent = this.footerText
    this.footer.classList.add("fade-in")

  }
  beforeToSend() {

    this.result = {}
    this.result.artifact = this.artifactName
    this.result.time = this.seconds
    this.result.sumary = this.qualifySumary
    this.result.date = new Date()




  }
  getQuestionList() {
    return this.preguntas
  }
  // validate() {


  //   // Implementación de validación de contenedores
  // }

}
class ContenedorCompuesto extends Contenedor {
  constructor(rows = null, cols = null) {
    super()
    this.preguntas = [];

    this.rows = rows
    this.cols = cols
    this.table = document.createElement("table");
    this.addTable(this.table)
    this.auxNodo;
    this.auxEvent;
    this.auxRow = 1
    this.auxCols = 1
    this.originalNodo

    this.defModal();

  }

  addCell(question, row, col) {

    this.addComponente(question)

  }

  showQuestionModal() {

    if (this.modal.style.display == 'block') {
      this.modal.style.display = 'none'

    }
    else {
      this.modal.style.display = 'block'
      this.modal.querySelector('.modal').style.display = 'flex'
    
      // this.auxNodo.querySelector("input").focus()

      const inputElement = this.auxNodo.querySelector("input");
      const mathFieldElement = this.auxNodo.querySelector("math-field");
      
      if (inputElement) {

        this.auxNodo.querySelector("input").focus()
          // Código para elementos input
      } else if (mathFieldElement) {

          this.auxNodo.mathVirtualKeyboardPolicy = "manual";
          
          this.auxNodo.addEventListener("focusin", () => mathVirtualKeyboard.show());
          this.auxNodo.addEventListener("focusout", () => mathVirtualKeyboard.hide());
          
          this.auxNodo.addEventListener("mount", (e) => {

              mathFieldElement.focus();
              mathVirtualKeyboard.show();
          });

          mathFieldElement.addEventListener('focus', () => { 
            mathVirtualKeyboard.layouts = ["minimalist"];
            mathVirtualKeyboard.visible = true;
          });
      }



    }
  }
  // addComponente(componente, coord) {

  //   let callProcedural = false;
  //   if (this.config === 'procedural') {
  //     callProcedural = true;
  //   }

  //   let arrayAux = [];
  //   const tiposDeseados = ['math-field', 'input', 'textfield', 'select', 'input[type="radio"]:checked'];


  //   componente.forEach(rowElement => {

  //     let row = document.createElement('tr')
  //     this.table.appendChild(row)
  //     rowElement.forEach(colElement => {

  //       let col = document.createElement('td')
  //       row.appendChild(col)


  //       colElement.forEach(question => {
  //         question.questionObject.callProcedural = callProcedural

  //         if (callProcedural) {
  //           this.procedural(1, this.preguntas, question)
  //         }
  //         let nodo = question.questionObject.crearNodo("simple")
  //         col.appendChild(nodo[0])



  //         let eventoInput = () => {
  //           if (screen.width <= 500) {
  //             this.fillTheModal(nodo[0])
  //             this.showQuestionModal()

  //           }
  //         }

  //         tiposDeseados.forEach((tipo) => {

  //           const nodoHijo = nodo[0].querySelector(tipo);

  //           if (nodoHijo != null) {

  //             if (callProcedural) {
  //               this.procedural(2, this.preguntas, question)
  //             }
      


              

              
  //             this.auxEvent = eventoInput
  //             nodoHijo.addEventListener('click', this.auxEvent)
  //             arrayAux.push(nodoHijo)
  //             arrayAux.push(nodo[1])

  //             question.questionObject.updateValorIsChange(this)

  //             this.preguntas.push(question);
  //             arrayAux = []




  //             if (this.lastNode == null) {
  //               this.lastNode = question

  //             }
  //             else {
  //               this.lastNode.questionObject.metodoEnlazado = this.lastNode.questionObject.nextNodeTrigger.bind(question)
  //               this.lastNode = question


  //             }



  //           }

  //         })


  //       });


  //     });
  //   });

    

  // }

  addHeaderComponent (componente, isHeader =null) {
    let callProcedural = false;
    if (this.config === 'procedural') {
      callProcedural = true;
    }

    let arrayAux = [];
    const tiposDeseados = ['math-field', 'input', 'textfield', 'select', 'input[type="radio"]:checked'];


    componente.forEach(rowElement => {

      let row = document.createElement('tr')
      this.table.appendChild(row)
      rowElement.forEach(colElement => {
        let col;
if(isHeader ==true){
  col = document.createElement('th')

}
else{
 col = document.createElement('td')

}
        row.appendChild(col)


        colElement.forEach(question => {
          question.questionObject.callProcedural = callProcedural

          if (callProcedural) {
            this.procedural(1, this.preguntas, question)
          }
          let nodo = question.questionObject.crearNodo("simple")
          col.appendChild(nodo[0])



          let eventoInput = () => {
            if (screen.width <= 500) {
              this.fillTheModal(nodo[0])
              this.showQuestionModal()

            }
          }

          tiposDeseados.forEach((tipo) => {

            const nodoHijo = nodo[0].querySelector(tipo);

            if (nodoHijo != null) {

              if (callProcedural) {
                this.procedural(2, this.preguntas, question)
              }
      
             if(nodoHijo.querySelector('option') == null){
              this.auxEvent = eventoInput

              nodoHijo.addEventListener('click', this.auxEvent)

             }
              arrayAux.push(nodoHijo)
              arrayAux.push(nodo[1])

              question.questionObject.updateValorIsChange(this)

              this.preguntas.push(question);
              arrayAux = []




              if (this.lastNode == null) {
                this.lastNode = question

              }
              else {
                this.lastNode.questionObject.metodoEnlazado = this.lastNode.questionObject.nextNodeTrigger.bind(question)
                this.lastNode = question


              }



            }

          })


        });


      });
    });

    

  }
  fillTheModal(originalNodo) {
  

    let modalQuestion = this.modal.querySelector('.modal_question')
    let lastNode = modalQuestion.querySelector('*')

    if (lastNode && lastNode.parentNode === modalQuestion) {
      modalQuestion.removeChild(lastNode) 
    }

    this.auxNodo = originalNodo.cloneNode(true)
    this.originalNodo = originalNodo
    // console.log(this.auxNodo)
    this.auxNodo.layouts = ['minimalist']
    modalQuestion.appendChild(this.auxNodo)

    modalQuestion.addEventListener('input', () => {

      try {
        this.originalNodo.querySelector('input').value = this.auxNodo.querySelector('input').value
      } catch (e) {
        this.originalNodo.querySelector('math-field').value = this.auxNodo.querySelector('math-field').value
      }
    })


  }

  addTable() {

    for (var i = 0; i < this.rows; i++) {
      let row = document.createElement("tr");
      for (var j = 0; j < this.cols; j++) {
        let cell = document.createElement("td");
        row.appendChild(cell);

      }
      this.table.appendChild(row);




    }
    this.cotentContainer.appendChild(this.table)


  }


}
class ContenedorInline extends Contenedor {
  constructor() {
    super()
    this.cotentContainer = document.createElement("div")
    this.cotentContainer.classList.add("quiz__inlineDisplay")
    this.elementOrder = 1
    this.setOfNodes = document.createElement("div")
  }


  addComponente(componente) {
    this.setOfNodes.classList.add("setOfNodes")

    let callProcedural = false;
    if (this.config === 'procedural') {
      callProcedural = true;
    }
    let arrayAux = []


    const tiposDeseados = ['math-field', 'input', 'textfield', 'select', 'input[type="radio"]:checked'];
    if (callProcedural) {
      this.procedural(1, this.preguntas, componente)
      
    }
    if (this.preguntas.length < 1) {
      componente.isValidable = true
    }
    else {
      componente.isValidable = false

    }
    componente.questionObject.callProcedural = callProcedural

    let nodoData = componente.questionObject.crearNodo("simple")
    nodoData[0].classList.add("simpleNode")


    nodoData[0].setAttribute('data-question', `${this.preguntas.length}`)

    try {
      nodoData[0].querySelector("input").classList.add("inlineDisplay--input")

    } catch (error) {

    }


    nodoData[0].setAttribute("style", `order: ${this.elementOrder};  margin: 0.1rem; display: flex;align-items:center`)



    if (this.setOfNodes.childNodes.length < 2){
      this.setOfNodes.appendChild(nodoData[0])
    }
    else{
      this.cotentContainer.appendChild(this.setOfNodes)

      this.setOfNodes =  document.createElement("div")
      this.setOfNodes.classList.add("setOfNodes")
      this.setOfNodes.appendChild(nodoData[0])
    }
    this.cotentContainer.appendChild(this.setOfNodes)


    this.elementOrder += 1
    tiposDeseados.forEach((tipo) => {
      
      const nodoHijo = this.cotentContainer.querySelector(tipo);



      if ((nodoHijo != null) && (!(componente.questionObject instanceof TextoGenerator))) {
        if (callProcedural) {
          this.procedural(2, this.preguntas, componente)
        }

        window.addEventListener('resize', () => {
          if ((nodoData[0].offsetLeft <= 80)) {
            nodoData[0].classList.add('quiz__default__input--full')
          }
          else {
            nodoData[0].classList.remove('quiz__default__input--full')

          }
        })



        arrayAux.push(nodoHijo)
        arrayAux.push(nodoData[1])





        if (this.lastNode == null) {
          this.lastNode = componente

        }
        else {
          this.lastNode.questionObject.metodoEnlazado = this.lastNode.questionObject.nextNodeTrigger.bind(componente)
          this.lastNode = componente


        }


        this.preguntas.push(componente);

        arrayAux = []
      }




    })



    if (!(componente.questionObject instanceof TextoGenerator)) {
      componente.questionObject.updateValorIsChange(this)

    }

  }

  validationProcess() {
    this.qualifySumary.total = this.preguntas.length
    this.qualifySumary.correct = 0
    this.qualifySumary.incorrect = 0
    this.qualifySumary.forAnswer = 0
    let auxValidationValue
    this.preguntas.forEach(questionElement => {


      if (auxValidationValue == true) {
        questionElement.isValidable = true
  
        auxValidationValue = false

      }

      let vali = questionElement.questionObject.validate()
 
      if (vali) {
        this.qualifySumary.correct += 1
        auxValidationValue = true


      }
      else if (vali === false) {
        this.qualifySumary.incorrect += 1
        auxValidationValue = true

      }
      else if (vali == undefined) {
        this.qualifySumary.forAnswer += 1
      }



    });
    this.beforeToSend()
  }


  validationHandler() {
    console.log(this.preguntas)

  }


}
class Pregunta {
  constructor(texto, validacion) {
    this.texto = texto;
    this.validacion = validacion;
    this.validationState = undefined
    this.nextNodeTrigger = this.changeValidableState
    this.isValidable = true
    this.metodoEnlazado;
    this.lastValue;
    this.funcionDeActualizacionEnContenedor
    this.modal;

  }
  updateValorIsChange(objeto) {
    if (objeto.isChanged == false) {
      this.funcionDeActualizacionEnContenedor = objeto.updateValor.bind(objeto, true);
    }
  }
  set setNodeTrigger(newState) {
    this.nextNodeTrigger = newState
  }

  changeValidableState() {
    this.isValidable = !(this.isValidable)
    if (this.isValidable) {

      try {
        this.nodo.disabled = false;
        this.nodo.removeAttribute("read-only");
        this.nodo.removeAttribute("only-read");
        this.nodo.classList.remove("quiz__default__input__math-field--disabled");
        const radioInputs = this.nodo.querySelectorAll('input[type="radio"]');
        radioInputs.forEach((element) => {
          element.disabled = false;
        });
      } catch (error) {
        console.error("Error:", error);
      }
    }

  }
  basicValidation() {

    if (this.nodo.value === "") {
      this.validationState = undefined
    }
    else if (this.nodo.value == this.validacion) {
      this.validationState = true

    }
    else if (this.nodo.value != this.validacion)
      this.validationState = false

    else {
      console.error('Revisa el estado de la propiedad "validationState" (No está contemplado)')
    }



    this.qualify(this.validationState)
    return this.validationState

  }

  qualify(validationState) {
    if (validationState === true) {
      this.nodo.classList.remove("input__math-field--incorect")
      this.nodo.classList.add("input__math-field--success")

    }
    else if (validationState === false) {
      this.nodo.classList.remove("input__math-field--success")
      this.nodo.classList.add("input__math-field--incorect")

    }
    else {
      this.nodo.classList.remove("input__math-field--incorect")
      this.nodo.classList.remove("input__math-field--success")

    }

  }
reset(){
  let tempNodo 
if (this.nodo.querySelector('input[type ="radio"]:checked')){
tempNodo = this.nodo.querySelector('input[type ="radio"]:checked')
tempNodo.checked = false
tempNodo.classList.remove("input__radio--success")
tempNodo.classList.remove("input__radio--incorect")
}
else if (this.nodo?.selectedIndex){
  this.nodo.selectedIndex = 0
  this.nodo.classList.remove("input__math-field--incorect")
  this.nodo.classList.remove("input__math-field--success")
}
else{
  this.nodo.value = ''
  this.nodo.classList.remove("input__math-field--incorect")
  this.nodo.classList.remove("input__math-field--success")
}

}

addNodeEvents(){
  this.nodo.addEventListener("blur", () => {
    if (this.isValidable == true){
      this.metodoEnlazado()

    }
  })

  this.nodo.addEventListener("input",()=>{
    this.funcionDeActualizacionEnContenedor()
  })
  if(this.isValidable === true){

  
  this.nodo.addEventListener("blur", () => {
    this.metodoEnlazado

  })
}

}
  // ContadorPreguntas
}
class PreguntaSimple extends Pregunta {

  constructor(element) {

    super(element.texto, element.validacion);
    this.arrayAux1 = []
    this.texto = element.data.label
    this.validacion = element.data.condition
    this.metodoValidacion = element.data.questionType
    this.myType = element.type


  }
  reset() {
    this.nodo.value = ''
    this.nodo.classList.remove("input__math-field--success")
    this.nodo.classList.remove("input__math-field--incorect")

  }
  crearNodo(myType = null) {
    const label = document.createElement('label');
    label.innerHTML =this.texto 
    this.nodo = document.createElement('input');

    this.nodo.classList.add("quiz__default__input")
    this.nodo.setAttribute('type', 'text'); // O tipo de input según la necesidad

    const contenedor = document.createElement('div');

    if (myType == "simple") {
      this.nodo.classList.add("input--simple")
    }
    else {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")
      // Retornar el contenedor que contiene la pregunta

    }


    if (this.isValidable == false) {
      this.nodo.setAttribute('disabled', 'true')
      this.nodo.setAttribute('read-only', 'true')
      this.nodo.classList.add("quiz__default__input__math-field--disabled")
    
    }


    this.addNodeEvents()

    contenedor.appendChild(this.nodo);

    this.arrayAux1.push(contenedor)
    this.arrayAux1.push(this.validacion)





    return this.arrayAux1;
  }

  validate() {
    if (this.lastValue != this.nodo.value) {

      this.funcionDeActualizacionEnContenedor()

      this.lastValue = this.nodo.value
      if (this.metodoValidacion == "basic") {

        return this.basicValidation()


      }
      else if (this.metodoValidacion == 1) {
        console.log("this is an alternative")
      }
    }
    else {
      console.log('No ha cambiado nada de la respuesta')
      return this.basicValidation()

    }





  }

}
class TextoGenerator {
  constructor(contenido) {
    this.arrayAux1 = []
    this.texto = contenido.data
    this.formato = contenido.formato


  }
  crearNodo(type = null) {
    this.nodo

    // Crear el nodo de la pregunta en el DOM

    this.nodo = document.createElement('p');
    this.nodo.classList.add("inline_text")
    this.nodo.innerHTML = this.texto
    if(this?.formato ){
this.nodo.setAttribute("style",`font-size:${this.formato.fontSize}; font-style:${this.formato.fontStyle}`)    
    }


    // Retornar el contenedor que contiene la pregunta


    this.arrayAux1.push(this.nodo)
    this.arrayAux1.push(this.validacion)


    return this.arrayAux1;
  }
}
class MathField extends Pregunta {
  constructor(quiestionData) {
    super();
    this.arrayAux1 = []
    this.popUp = quiestionData.data.popUp
    this.texto = quiestionData.data.label
    this.validacion = quiestionData.data.condition
    this.metodoValidacion = quiestionData.data.questionType
    this.keyBoardProfile = quiestionData.keyboardProfile

    this.myType = null

  }
  crearNodo(myType = null) {
    this.nodo
    const label = document.createElement('label');
    label.innerHTML =this.texto 
    this.nodo = document.createElement('math-field');
    this.nodo.classList.add("quiz__default__input", "quiz__default__input__math-field")
    

    const contenedor = document.createElement('div');
    if (myType == "simple") {
      this.nodo.classList.add("input--simple")
      contenedor.classList.add("QuestionElement")
    }
    else {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")
      // Retornar el contenedor que contiene la pregunta
    }
   this.addNodeEvents()
    contenedor.appendChild(this.nodo);
    this.arrayAux1.push(contenedor)
    this.arrayAux1.push(this.validacion)



    return this.arrayAux1;
  }
  addNodeEvents(){

    if (this.isValidable == false) {
      this.nodo.setAttribute('read-only', 'true')
      this.nodo.setAttribute('disabled', 'true')

      this.nodo.classList.add("quiz__default__input__math-field--disabled")

    }else{
      this.nodo.mathVirtualKeyboardPolicy = "manual";


      this.nodo.addEventListener("focusin", () => {
        // this.nodo.layouts = this.keyBoardProfile

        mathVirtualKeyboard.show()});
      this.nodo.addEventListener("focusout", () =>
        mathVirtualKeyboard.hide()

      );


      this.nodo.addEventListener('focus', (e) => { 
        e.preventDefault();
         mathVirtualKeyboard.layouts = [this.keyBoardProfile[0]];
        mathVirtualKeyboard.visible = true;
      });
    }
  //   if (this.popUp != undefined){
  //     var timer;
  //     var touchduration = 800; //length of time we want the user to touch before we do something
  //     let bubble = document.createElement("div")
  //     bubble.classList.add("bubble")
  //     bubble.textContent="hola"
  //     bubble.setAttribute("style",".bubble{background:#139ee0; color:#fff; padding:7px 15px; border-radius:3px; width:350px;display:hidden}")

  //   let touchstart1 =((e)=> {
  //      // e.preventDefault();
  //      this.nodo
  //       if (!timer) {
  //           timer = setTimeout(onlongtouch, touchduration);
  //       }
  //   })
  //   let onlongtouch = ()=> { 
      
  //     timer = null;
  //     // document.getElementById('ping').innerText+='ping\n'; 
  // };
  // let  touchend = (() => {
  //   //stops short touches from firing the event
  //   if (timer) {
  //       clearTimeout(timer);
  //       timer = null;
  //   }
  // })
  
  // this.nodo.addEventListener("touchstart",touchstart1,false)
  // this.nodo.addEventListener("touchend",touchend,false)
  
  
  
  //   }





    if(this.callProcedural == true){
      this.nodo.addEventListener("blur", () => {
        if (this.isValidable == true){
          this.metodoEnlazado()
    
        }
    

      })

      this.nodo.addEventListener("click", () => {

        this.metodoEnlazado()
  
  
      })
    }
    
    
        this.nodo.addEventListener("input",()=>{
          this.funcionDeActualizacionEnContenedor()
    
        })
  }
  calculate(valor) {
    var ce = new ComputeEngine.ComputeEngine({
      numericMode: 'machine'
    });
    var result = ce.parse(valor).N().valueOf();
    return result;
  }

  gInterPoint(value, compare) {
    var noise = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0.1;
    var minDecimal = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 2;
    if (typeof value === 'number' && typeof compare === 'number') {
      if (parseFloat(value.toFixed(minDecimal)) <= parseFloat(compare.toFixed(minDecimal)) + noise && parseFloat(value.toFixed(minDecimal)) >= parseFloat(compare.toFixed(minDecimal)) - noise) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  validate() {
    if (this.metodoValidacion == "basic") {

      return this.basicValidation()
    }
    else if (this.metodoValidacion == 1) {

      return this.expressionValidation()
    }
    else if (this.metodoValidacion == "operate") {
      return this.operatorValidation()
    }

  }



  expressionValidation() {

    /**
     * Aquí se valida si la expresión ingresada esté presente en una o mas alternativas de estas
     * Son expresiones Estáticas, cadenas de string que no pueden resolverse y no tienen variaciones por décimas
     * **/
    if (Array.isArray(this.validacion)) {
      this.validacion.some((element) => element === this.nodo.value)

    }
    else {
      this.nodo.value == this.validacion
    }
  }

  operatorValidation(noise = 0) {
    console.log("OperatorValidation")
    /**
     * Aquí se validan Valores numéricos que pueden o no soportar pequeñas o grandes variaciones así como varias respuestas válidas
     */

    let latexExpression = this.nodo.value

    if (latexExpression == '') {
      return undefined
    }
    let calculated = this.calculate(latexExpression)
    // console.log("CALCULATED", calculated)
    // if (calculated ==45){
    //   this.validationState = true
    // }

    //this.qualify(this.validationState)




    if (noise < 1) {
      console.log("No se tolera inexactitud");
      if (Array.isArray(this.validacion)) {
        // Comprobar si algún elemento coincide con el valor calculado
        const isCalculatedValid = this.validacion.some((element) => {
          return this.gInterPoint(calculated, element, noise);
        });
        // Realizar acciones en función del resultado
        if (isCalculatedValid) {
          this.validationState = true
          this.qualify(this.validationState)

          return this.validationState    // Realizar alguna acción si el cálculo no es válido
        }
        this.validationState = false      // Realizar alguna acción si el cálculo no es válido
        this.qualify(this.validationState)
        return this.validationState    // Realizar alguna acción si el cálculo no es válido
      }
      if (this.gInterPoint(calculated, this.validacion, noise)) {

        this.validationState = true      // Realizar alguna acción si el cálculo no es válido
        this.qualify(this.validationState)

        return this.validationState
      }

      this.validationState = false      // Realizar alguna acción si el cálculo no es válido
      this.qualify(this.validationState)

      return this.validationState

    }





  }

  operatorByOrder() {
    /**
     * Validación de valores u expresiones separadas por algún identificador, vease ("," , ";") y se proceden a resolver y 
     * verificar si coinciden con los valores esperados y/o aproximados
     */

  }

}
class Select extends Pregunta {
  constructor(quiestionData) {

    super()
    this.optionList = quiestionData.data.optionList
    this.condition = quiestionData.data.condition
    this.texto = quiestionData.data.label
  }

  validate() {
    // this.updateValor()


    if ((this.lastValue == this.nodo.selectedIndex) && (this.nodo.selectedIndex ==0)) {
      console.warn('mismo valor')
      // return this.basicValidation()

    } else {

      if (this.metodoValidacion == "basic" || this.metodoValidacion == undefined) {

        this.funcionDeActualizacionEnContenedor()
        this.qualify(this.validationState)


        this.lastValue = this.nodo.selectedIndex
        return this.basicValidation()
      }

    }

  }
  basicValidation() {
    if (this.nodo.selectedIndex == this.condition) {
      this.validationState = true
      this.qualify(this.validationState)

      return true
    }
    else {
      this.validationState = false
      this.qualify(this.validationState)

      return false
    }
  }

  crearNodo(myType = null) {
    this.nodo
    this.arrayAux1 = [];
    this.profile = 'simple'
    const label = document.createElement('label');
    label.textContent = this.texto;
    this.nodo = document.createElement('select');


    this.optionList.forEach((element) => {
      const option = document.createElement("option");
      option.value = element;
      option.text = element;
      this.nodo.appendChild(option);
    })

      this.addNodeEvents()

    if (this.isValidable == false) {
      this.nodo.disabled =true
      this.nodo.classList.add("quiz__default__input__math-field--disabled")
    }


    this.nodo.classList.add("quiz__default__input")
    this.nodo.classList.add("quiz__default__select")

    const contenedor = document.createElement('div');


    if (myType == 'complete') {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")
    }

    if (myType == "simple") {
      this.nodo.classList.add("input--simple")
    }
    else {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")


    }
    contenedor.appendChild(this.nodo);
    this.arrayAux1.push(contenedor)
    this.arrayAux1.push(this.validacion)


    this.lastValue = this.nodo.selectedIndex
    return this.arrayAux1;
  }

  addNodeEvents(){
    this.nodo.addEventListener("input",()=>{
      this.funcionDeActualizacionEnContenedor()
    })
    this.nodo.addEventListener("input", () => {
      this.metodoEnlazado()

    })

    this.nodo.addEventListener("focusin", () => {
      // mathVirtualKeyboard.layouts = (keyboardsLayouts[this.keyBoardProfile] || this.keyBoardProfile);
    });
  }
}
class Radio extends Pregunta {
  constructor(quiestionData) {
    super()
    this.optionList = quiestionData.data.optionList
    this.condition = quiestionData.data.condition
    this.texto = quiestionData.data.label
    this.qustionID = quiestionData.questionId

  }
  validate() {
    if (this.lastValue == this.nodo.querySelector('input[type ="radio"]:checked')) {
      console.warn('mismo valor')
      this.lastValue = this.nodo.querySelector('input[type ="radio"]:checked')
      return this.basicValidation()
    } else {

      if (this.metodoValidacion == "basic" || this.metodoValidacion == undefined) {

        this.funcionDeActualizacionEnContenedor()
        this.qualify(this.validationState)


        this.lastValue = this.nodo.querySelector('input[type ="radio"]:checked')
        return this.basicValidation()
        
      }

      

    }

  }
  basicValidation() {
    //console.log((this.nodo.querySelector('input[type ="radio"]:checked').value))

    if (this.nodo.querySelector('input[type ="radio"]:checked').value == this.condition) {
      this.validationState = true
      this.qualify(this.validationState)

      return true
    }
    else {
      this.validationState = false
      this.qualify(this.validationState)

      return false
    }
  }
  crearNodo(myType = null) {
    this.nodo
    this.arrayAux1 = [];
    this.profile = 'simple'
    const label = document.createElement('label');
    label.textContent = this.texto;
    this.nodo = document.createElement('div');
    this.nodo.classList.add("radioset__container")
    this.optionList.forEach((element) => {
      let radioSet = document.createElement("div")
      radioSet.classList.add('radioSet')
      let label = document.createElement('label');
      const option = document.createElement("input");
      label.textContent = element
      option.value = element
      label.setAttribute('for', `${this.qustionID}`);
      option.setAttribute('name', `${this.qustionID}`);
      option.classList.add("input__radio")

      option.type = "radio";


      radioSet.appendChild(label)
      radioSet.appendChild(option)

  
      this.nodo.appendChild(radioSet);
    })


    if (this.isValidable == false) {
      this.nodo.querySelectorAll('input[type ="radio"]').forEach((element)=>{
        element.disabled = true
      })
      this.nodo.disabled =true
    }

 
    const contenedor = document.createElement('div');


    if (myType == 'complete') {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")
    }

    if (myType == "simple") {
      this.nodo.classList.add("input--simple")
    }
    else {
      contenedor.appendChild(label);
      contenedor.classList.add("QuestionElement")


    }


    contenedor.appendChild(this.nodo);
    this.arrayAux1.push(contenedor)
    this.arrayAux1.push(this.validacion)


this.addNodeEvents()


    this.lastValue = this.nodo.selectedIndex
    return this.arrayAux1;
  }
addNodeEvents(){
  this.nodo.addEventListener("input", () => {
    this.metodoEnlazado()

  })

  this.nodo.addEventListener("input",()=>{
    this.funcionDeActualizacionEnContenedor()
  })
}


  qualify(validationState) {
    let radioNodeToValidate = this.nodo.querySelector('input[type ="radio"]:checked')
    let restOfNodes = this.nodo.querySelectorAll('input[type ="radio"]')


    if (validationState === true) {
      restOfNodes.forEach((element)=>{
        element.classList.remove("input__radio--incorect")
      })
     radioNodeToValidate.classList.add("input__radio--success")

    }
    else if (validationState === false) {

        restOfNodes.forEach((element)=>{
          element.classList.remove("input__radio--success")
        })

      radioNodeToValidate.classList.add("input__radio--incorect")

    }
    else {
    
        restOfNodes.forEach((element)=>{
          element.classList.remove("input__radio--success")
          element.classList.remove("input__radio--incorect")
            })

    

  }

}
  }

